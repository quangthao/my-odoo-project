# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import ValidationError

class SaleOrderInherit(models.Model):
    _inherit="sale.order"

    plan_sale_order_id = fields.Many2one('plan.sale.order', string='Plan sale order')
    plan_sale_order_ids = fields.One2many('plan.sale.order', 'sale_order_id', string='Plan sale order')

    def ActionViewPlanSaleOrder(self):
        self.ensure_one()
        action = self.env["ir.actions.act_window"]._for_xml_id("plan_sale_order.action_button_plan_sale_order")

        action["context"] = {
            "default_sale_order_id": self.id,
        }
        return action

    def action_confirm(self):
        plan_sale_order_ids = self.plan_sale_order_ids
        if plan_sale_order_ids:
            if len(plan_sale_order_ids) > 1:
                raise ValidationError(_("You have too many plan sale for this quotation."))
            else:
                if plan_sale_order_ids.state != 'approved':
                    raise ValidationError(_("This quotation plan sale doesn't approve."))
        else:
            raise ValidationError(_("This quotation hasn't plan sale."))

        # plans_sale = self.env['plan.sale.order'].search([('sale_order_id', '=', self.id)])
        # if len(plans_sale) == 1:
        #     if plans_sale.state != 'approved':
        #         raise ValidationError(_("This quotation plan sale doesn't approve."))
        # elif len(plans_sale) > 1:
        #     raise ValidationError(_("You have too many plan sale for this quotation."))
        # else:
        #     raise ValidationError(_("This quotation hasn't plan sale."))

        if self._get_forbidden_state_confirm() & set(self.mapped('state')):
            raise UserError(_(
                'It is not allowed to confirm an order in the following states: %s'
            ) % (', '.join(self._get_forbidden_state_confirm())))

        for order in self.filtered(lambda order: order.partner_id not in order.message_partner_ids):
            order.message_subscribe([order.partner_id.id])
        self.write(self._prepare_confirmation_values())

        # Context key 'default_name' is sometimes propagated up to here.
        # We don't need it and it creates issues in the creation of linked records.
        context = self._context.copy()
        context.pop('default_name', None)

        self.with_context(context)._action_confirm()
        if self.env.user.has_group('sale.group_auto_done_setting'):
            self.action_done()
        return True