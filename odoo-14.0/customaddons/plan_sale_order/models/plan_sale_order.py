# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from datetime import datetime, date, timedelta

class PlanSaleOrder(models.Model):
    _name = "plan.sale.order"
    _inherit = [ 'mail.thread', 'mail.activity.mixin']

    name = fields.Text(string='Plan sale order')
    sale_order_id = fields.Many2one('sale.order', string='Sale order', required=True)
    information = fields.Text(string='Information')
    state = fields.Selection([
        ('draft', 'New Draft'),
        ('waitting', 'Waitting'),
        ('approved', 'Approved'),
        ('refused', 'Refused'),
    ], string='Status', readonly=True, default='draft', compute="get_state", store=True)
    approver_ids = fields.One2many('list.of.approvers', 'plan_sale_order_id', string='List of approvers')
    # sale_order_ids = fields.One2many('sale.order', 'plan_sale_order_id', string='Sale order',)

    _sql_constraints = [
        ('sale_order_uniq', 'UNIQUE (sale_order_id)',
         'Sale order must be unique.'),
    ]

    # gửi noti tới danh sách người duyệt
    def submit_function(self):
        self.state = 'waitting'

        # gửi noti tới người duyệt
        now = datetime.now() + timedelta(days=0)
        date_now = now.date()
        note = 'Dear approver'
        activity_type_id = self.env.ref('mail.mail_activity_data_todo').id

        approver_ids = self.approver_ids
        for approver in approver_ids:
            if approver.partner_id.user_ids:
                self.env['mail.activity'].create({
                    'activity_type_id': activity_type_id,
                    'note': note,
                    'user_id': approver.partner_id.user_ids.id,
                    'res_id': self.id,
                    'res_model_id': self.env.ref('plan_sale_order.model_plan_sale_order').id,
                    'date_deadline': date_now,
                })


    @api.depends('approver_ids.state')
    def get_state(self):
        approvers = self.approver_ids
        x = 0
        for approver in approvers:
            if approver.state == 'refused':
                self.state = 'refused'
                x = 1
                break
            if approver.state != 'approved':
                x = 1
                break
        if x == 0:
            self.state = 'approved'

