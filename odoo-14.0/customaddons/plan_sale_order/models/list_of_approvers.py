# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError
from datetime import datetime, date, timedelta

class ListOfApproves(models.Model):
    _name = "list.of.approvers"

    plan_sale_order_id = fields.Many2one('plan.sale.order', string='Plan sale order')
    partner_id = fields.Many2one('res.partner', string='Approver')
    state = fields.Selection([
        ('waitting', 'Waitting'),
        ('approved', 'Approved'),
        ('refused', 'Refused'),
    ], string='Status', readonly=True, default='waitting')

    def approve_function(self):
        # kiểm tra người đang đăng nhập có được quyền chấp nhận hay không
        if self.partner_id.user_ids.id == self.env.uid:
            self.state = 'approved'
        else:
            raise UserError(_("You can not approve."))

        # noti về cho người tạo
        now = datetime.now() + timedelta(days=0)
        date_now = now.date()
        note = 'Dear creater'
        activity_type_id = self.env.ref('mail.mail_activity_data_todo').id
        if self.plan_sale_order_id.create_uid.id:
            self.env['mail.activity'].create({
                'activity_type_id': activity_type_id,
                'note': note,
                'user_id': self.plan_sale_order_id.create_uid.id,
                'res_id': self.plan_sale_order_id.id,
                'res_model_id': self.env.ref('plan_sale_order.model_plan_sale_order').id,
                'date_deadline': date_now,
            })

    def refuse_function(self):
        # kiểm tra người đang đăng nhập có được quyền từ chối hay không
        if self.partner_id.user_ids.id == self.env.uid:
            self.state = 'refused'
        else:
            raise UserError(_("You can not refuse"))

        # noti về cho người tạo
        now = datetime.now() + timedelta(days=0)
        date_now = now.date()
        note = 'Dear creater'
        activity_type_id = self.env.ref('mail.mail_activity_data_todo').id
        if self.plan_sale_order_id.create_uid.id:
            self.env['mail.activity'].create({
                'activity_type_id': activity_type_id,
                'note': note,
                'user_id': self.plan_sale_order_id.create_uid.id,
                'res_id': self.plan_sale_order_id.id,
                'res_model_id': self.env.ref('plan_sale_order.model_plan_sale_order').id,
                'date_deadline': date_now,
            })
