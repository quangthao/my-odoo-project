{
    'name': "Custom button",
    'summary': """Custom button""",
    'description': """Custom button""",
    'author': "Quang",
    'website': "",
    'category': 'Quang',
    'version': '1.2',
    'sequence': 2,
    'depends': [
        'my_library',
    ],
    'data': [
        'views/invisible_button.xml',

    ],
    'qweb': [],
    'installable': True,
    'application': True,
}