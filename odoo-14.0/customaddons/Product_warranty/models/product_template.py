# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError
import time
from datetime import datetime,date
from dateutil import relativedelta

class ProductTemplateInherit(models.Model):
    _inherit="product.template"

    has_warranty = fields.Boolean(string='Has warranty', default=False)
    product_warranty = fields.Text(compute='_compute_product_warranty', string='Product warranty code', readonly=True, store=True)
    date_from = fields.Date(string='Date From',
                            default=time.strftime('%Y-%m-%d'), readonly=False, help="Start date")
    date_to = fields.Date(string='Date To', readonly=False, help="End date",
                          default=time.strftime('%Y-%m-%d'), )
    number_of_valid_days = fields.Integer(compute='_compute_product_warranty', readonly=True, store=True)
    discount_percent = fields.Float(string='Discount (%)', digits='Discount',compute='_compute_product_warranty', readonly=True, store=True)

    @api.constrains('date_from', 'date_to')
    def constrains_date(self):
        for each in self:
            if each.date_from > each.date_to:
                raise UserError(_('Date To must be greater than Date From'))

    @api.depends('date_to', 'date_to', 'has_warranty')
    def _compute_product_warranty(self):
        for each in self:
            if each.has_warranty:
                if each.date_from <= each.date_to:
                    # compute product_warranty
                    str_date_from = str(each.date_from)
                    str_date_to = str(each.date_to)
                    string = 'PWR/' +  str_date_from[8:10] + str_date_from[5:7] + str_date_from[2:4] + '/' +  \
                             str_date_to[8:10] + str_date_to[5:7] + str_date_to[2:4]
                    each.product_warranty = string

                    # compute number_of_valid_days
                    date_format = "%Y-%m-%d"
                    d0 = datetime.strptime(str(each.date_from), date_format)
                    d1 = datetime.strptime(str(each.date_to), date_format)
                    delta = abs(d0 - d1)
                    each.number_of_valid_days = delta.days

                    # compute discount percent
                    today = date.today()
                    if each.date_to > today:
                        each.discount_percent = 0.0
                    else:
                        each.discount_percent = 10.0
            else:
                each.discount_percent = 10.0

    def button_print(self):

        y = str(self.date_to)
        string_date_to = y[8:10] + y[5:7] + y[2:4]
        print('heeeeeeeeeeeeeeeeee')

        # print(y)
        # print(string_date_to)
        date_format = "%Y-%m-%d"
        d0 = datetime.strptime(str(self.date_from), date_format)
        d1 = datetime.strptime(str(self.date_to), date_format)
        delta = abs(d0 - d1)
        # print (delta.days)

        today = date.today()
        d2 = today.strftime(date_format)
        print(today)
        print(d2)
        print(self.date_to)
        if self.date_to > today:
            print('con thoi han bao hanh')
        else:
            print('het han roi')


