from odoo import models, fields, api

class LibraryBookCopy(models.Model):

    _name = "library.book.copy"
    _inherit = "library.book"
    _description = "Library Book's Copy"

    def name_get(self):
        result = []
        for record in self:
            rec_name = "%s (%s)" % (record.name, record.date_release)
        result.append((record.id, rec_name))
        return result