from odoo import models, fields, api, _
from odoo.exceptions import ValidationError

class LibraryBookRent(models.Model):
    _name = 'library.book.rent'

    book_id = fields.Many2one('library.book', string='Book', required=True)
    borrower_id = fields.Many2one('res.partner', 'Borrower', required=True, domain="[('name','=', 'Addison Olson')]")
    crm_id = fields.Many2one('crm.lead', 'CRM', required=True,)
    state = fields.Selection([('ongoing', 'Ongoing'), ('returned', 'Returned'), ('lost', 'Lost')],'State', default='ongoing',required=True)
    rent_date = fields.Date(default=fields.Date.today)
    return_date = fields.Date()

    color = fields.Integer()
    popularity = fields.Selection([
        ('no', 'No Demand'),
        ('low', 'Low Demand'),
        ('medium', 'Average Demand'),
        ('high', 'High Demand')], default="no")
    tag_ids = fields.Many2many('library.rent.tag', string='Rent tag')
    tag_ids1 = fields.One2many('library.rent.tag', 'book_rent_id', string='One2many')
    tag_ids2 = fields.One2many('library.rent.tag', 'book_rent_id', string='One2many')

    @api.model
    def _default_rent_stage(self):
        Stage = self.env['library.rent.stage']
        return Stage.search([], limit=1)
    @api.model
    def _group_expand_stages(self, stages, domain, order):
        return stages.search([], order=order)

    stage_id = fields.Many2one(
        'library.rent.stage',
        default=_default_rent_stage,
        group_expand='_group_expand_stages'
    )

    def book_lost(self):
        self.ensure_one()
        self.sudo().state = 'lost'
        # book_with_different_context = self.book_id.with_context(avoid_deactivate=True)
        new_context = self.env.context.copy()
        new_context.update({'avoid_deactivate': True})
        book_with_different_context = self.book_id.with_context(new_context)
        book_with_different_context.sudo().make_lost()

    def book_return(self):
        for rec in self:
            self.sudo().state = 'returned'

    @api.constrains('rent_date', 'return_date')
    def _constraint_date(self):
        for rec in self:
            if rec.rent_date > rec.return_date:
                raise ValidationError(_("Date return must be larger than rent date."))

class LibraryRentStage(models.Model):

    _name = 'library.rent.stage'
    _order = 'sequence,name'

    name = fields.Char()
    sequence = fields.Integer()
    fold = fields.Boolean()
    book_state = fields.Selection(
    [('available', 'Available'),
    ('borrowed', 'Borrowed'),
    ('lost', 'Lost')],
    'State', default="available")

class LibraryRentTags(models.Model):

    _name = 'library.rent.tag'

    name = fields.Char()
    color = fields.Integer()
    book_rent_id = fields.Many2one('library.book.rent')