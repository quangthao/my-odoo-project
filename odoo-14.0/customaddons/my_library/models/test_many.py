from odoo import models, fields, api, _
from odoo.exceptions import ValidationError

class LibraryBookRent(models.Model):
    _name = 'test.many'

    book_id = fields.Many2one('library.book', string='Book', required=True)
    borrower_id = fields.Many2one('res.partner', 'Borrower', required=True)
    state = fields.Selection([('ongoing', 'Ongoing'), ('returned', 'Returned'), ('lost', 'Lost')],'State', default='ongoing',required=True)
    rent_date = fields.Date(default=fields.Date.today)
    return_date = fields.Date()

    @api.model
    def _default_rent_stage(self):
        Stage = self.env['library.rent.stage']
        return Stage.search([], limit=1)

    stage_id = fields.Many2one(
        'library.rent.stage',
        default=_default_rent_stage
    )