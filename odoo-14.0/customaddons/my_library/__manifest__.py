# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
'name': "My library",
'summary': "Manage books easily",
'description': """
Manage Library
==============
Description related to library.
""",
'author': "Your way",
'website': "",
'category': 'Uncategorized',
'version': '13.0.1',
'depends': ['base',
        'base_setup',
            'contacts',
            'project',
            ],
# sau khi cài đặt module mới cài được dòng sau
# 'post_init_hook': 'add_book_hook',
'data': [
    'security/groups.xml',
    'security/ir.model.access.csv',
    'security/security_rules.xml',
    'data/demo.xml',
    'data/data.xml',
    'data/library_stage.xml',
    'views/library_book.xml',
    'views/library_book_category.xml',
    'views/res_partner_inherit.xml',
    'views/library_book_copy.xml',
    'views/library_member.xml',
    'views/library_book_rent.xml',
    'wizard/LibraryRentWizard.xml',
    'wizard/LIbraryReturnWizard.xml',
    'views/project_task_kanban.xml',
    'views/res_config_setting.xml',
    'views/test_many.xml',
         ],
'demo': [],
}