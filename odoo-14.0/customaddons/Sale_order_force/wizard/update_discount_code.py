# -*- coding: utf-8 -*-
from odoo import api, fields, models

class UpdateDiscountCode(models.TransientModel):
    _name = "update.discount.code.wizard"

    discount_code = fields.Text(string='Discount code')

    def update_discount_code_for_customer(self):
        self.env['res.partner'].browse(self._context.get("active_ids")).update({'customer_discount_code':self.discount_code})

        return True