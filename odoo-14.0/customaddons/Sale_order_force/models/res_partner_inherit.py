# -*- coding: utf-8 -*-
from odoo import api, fields, models

class ResPartner(models.Model):
    _inherit="res.partner"

    customer_discount_code = fields.Text(string='Discount code')
    code_is_valid = fields.Boolean(compute='_compute_discount_number', readonly=True, store=True)
    discount_number = fields.Integer(compute='_compute_discount_number', store=True, readonly=True)
    has_discount_code = fields.Boolean(compute='_compute_discount_number', readonly=True, store=True)

    @api.depends('customer_discount_code')
    def _compute_discount_number(self):
        for rec in self:
            string = rec.customer_discount_code
            str1 = 'VIP_'
            if string:
                rec.has_discount_code = True
                if str1 in string:
                    str2 = string[4:].strip()
                    if str2:
                        try:
                            number = int(str2)
                            if 0 <= number <= 100:
                                rec.code_is_valid = True
                                rec.discount_number = number
                        except:
                            pass
            else:
                rec.has_discount_code = False

    @api.onchange('customer_discount_code')
    def onchange_discount_number(self):
        if self.customer_discount_code:
            self.has_discount_code = True
        else:
            self.has_discount_code = False
