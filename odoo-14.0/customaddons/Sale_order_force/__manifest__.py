{
    'name': "Sale Order Force",
    'summary': """Sale Order Force""",
    'description': """Sale Order Force""",
    'author': "Quang",
    'website': "",
    'category': 'WeUp',
    'version': '1.2',
    'sequence': 2,
    'depends': [
        'base',
        'crm',
        'sale_management',
    ],
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'wizard/update_discount_code_for_customer_view.xml',
        'views/res_partner_view.xml',
        'views/sale_order_inherit.xml',

    ],
    'qweb': [],
    'installable': True,
    'application': True,
}